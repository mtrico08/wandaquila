<?php
function get_instagram($user_id=28601952,$count=100,$width=190,$height=190){
	$client_id = '86829003d4d8483e84fba6eaf612cee4';
    $url = 'https://api.instagram.com/v1/users/'.$user_id.'/media/recent/?client_id='.$client_id.'&count='.$count;

	$filters = array();
    // Also Perhaps you should cache the results as the instagram API is slow
    $cache = './'.sha1($url).'.json';
    if(file_exists($cache) && filemtime($cache) > time() - 60*60){
        // If a cache file exists, and it is newer than 1 hour, use it
        $jsonData = json_decode(file_get_contents($cache));
    } else {
        $jsonData = json_decode((file_get_contents($url)));
        file_put_contents($cache,json_encode($jsonData));
    }

    $result = PHP_EOL;
    foreach ($jsonData->data as $key=>$value) {
		if(isset($value->caption)){
			$hashtag = $value->caption->text;
			$timestamp = $value->caption->created_time;
			$filters[] = $hashtag;
		} else {
			$hashtag = '';
			$timestampm = '';
		}
        $result .= "\t".'<li title="'.htmlentities($hashtag).' '.htmlentities(date("F j, Y, g:i a", $timestamp)).'"
                            href="'.$value->images->standard_resolution->url.'">
                          <img src="'.$value->images->low_resolution->url.'" 
						  alt="'.$hashtag.'" />
                          </li>'.PHP_EOL;
    }
    $result .= PHP_EOL;
    return $result;
}
?>