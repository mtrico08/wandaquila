$(document).ready(function(){
	var $container = $('#port').masonry({
	  columnWidth: '#port li',
	  itemSelector: 'li'
	});
	$container.imagesLoaded( function() {
		 $container.masonry();
	});
	// initialize
	var items=new Array();
	var these=new Array();
	items.push($('#port li'));
	$container.masonry('bindResize');
	//var msnry = $container.data('masonry');
	$('.filter').click(function(){
		var these = $('a', this).attr('id');
		var ownerclass = $('a', this).attr('class');
		if(!($(this).attr('title')=='clearfilters')&&!($(this).attr('title')=='clearlarger')){	
			$('#port .'+these).addClass(ownerclass);
			$('#port li').each(function() {
				if(!$(this).hasClass(these)){
					$(this).hide();
				}
			}); 
			$('#port .'+these).show();
		} else if($(this).attr('title')=='clearlarger'){	
			$('.'+ownerclass).removeClass('larger');
			$('#port .'+these).removeClass(ownerclass);
		} else {
			$('#port .'+these).removeClass(ownerclass);
			$('#port li').show();
		}
		$container.masonry('layout').masonry();
	});
	$container.on( 'click', 'img', function() {
		$( this ).parent('.clearfilters').toggleClass('larger');
		//$( this ).parent('.clearfilters').toggleClass(ownerclass);
		/*$('.clearfilters').not(this).parent('.clearfilters').removeClass('larger');*/
		$container.masonry();
  });
	$('.slow').click(function(){
		$('html, body').animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top - 260
		}, 600);
		if($('#nav').data('size') == 'big'){
			$('#nav').stop().animate({
					height: '60px'
			},300);
			$('#nav').data('size','small');		
		};
		return false;
	});
	$('#nav').data('size','small');
	$('.hamburger').click(function(){
			if($('#nav').data('size') == 'small'){
				$('#nav').stop().animate({
					height: '224px'
				},300);
				$('#nav').data('size','big');
			} else {
				$('#nav').stop().animate({
					height: '60px'
				},300);
				$('#nav').data('size','small');		
			}
	});
});
/*var onViewportChange = function(){
	if($('html').width() >= 768){
		$('#nav').data('size','small');
		$('#nav').height(60);
	}
};
var onViewportOrient = function(){
	if($('html').width() >= 480){
		var orientpoint = 'horizontal';
		} else {
		var orientpoint = 'vertical';
		}
};
$(window).on({
	orientationchange: function() {
		onViewportOrient();
		location.reload();
	},
	resize: function(){
		onViewportChange();
		onViewportOrient();
	}
});*/