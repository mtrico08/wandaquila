<?php require_once('instagram_class.php'); ?>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Cantora+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link type="text/css" rel="stylesheet" href="/css/style.css">
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=no">
		<meta name="robots" content="index, follow">
	</head>
	<body>
	<?php include_once("analyticstracking.php") ?>
		<div id="featured" class="nav">
			<div class="wrapper">
				<div id="logo">WandaQuila</div>
			</div>
		</div>
		<div id="sidebar" class="right">
			<?php include_once('sidebar.php'); ?>
		</div>
		<div id="portfolio">
			<ul id="port">
				<?php $instagram->listing(); ?>
			</ul>
		</div>
		<footer>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			<script src="/js/masonry.pkgd.min.js"></script>
			<script src="/js/imagesloaded.pkgd.min.js"></script>
			<script src="/js/scripts.js"></script>
		</footer>
	</body>
</html>