<?php
class instagram{

	private $user_id;
	private $client_id;
	public $count;
	public $cache;
	protected $url;
	public $jsonData;
	public $filters;
	protected $arg;
	public $metric;
	public $type;
	
	public function __construct() {
		$args = func_get_args();
		$this->username = $args[0];
		$this->client_id = $args[1];
		$this->counter = 200;
		$this->id_cache = 'https://api.instagram.com/v1/users/search?q='.$this->username.'&client_id='.$this->client_id;
		$this->cache_data = json_decode((file_get_contents($this->id_cache)));		
		$this->user_id = $this->cache_data->data[0]->id;
		$this->url = 'https://api.instagram.com/v1/users/'.$this->user_id.'/media/recent/?client_id='.$this->client_id.'&count='.$this->counter;
		// Caching existing
		$this->cache = './'.sha1($this->url).'.json';
		if(file_exists($this->cache) && filemtime($this->cache) > time() - 60*60){
			// If a cache file exists, and it is newer than 1 hour, use it
			$this->jsonData = json_decode(file_get_contents($this->cache));
		} else {
			$this->jsonData = json_decode((file_get_contents($this->url)));
			file_put_contents($this->cache,json_encode($this->jsonData));
		}
		return $this->jsonData;
	}
	
	public function cleaner($arg){
		$result = '';
		$arg = str_replace(' ','-', $arg);
		// replaces spaces with hyphens
		$arg = preg_replace('/[^A-Za-z0-9\-\!]/', '', $arg);
		//$arg = preg_split('/(?=[A-Z])/',$arg);
		$arg = preg_split('/[-]/',$arg);
		// now splits words by the hyphen to shorten them
		foreach ($arg as $str) {
			$str = strtolower($str);
			$str = ucfirst($str);
			$result .= $str;
		};
		$arg = $result;
		//$arg = str_replace('-','', $result);
		return $arg;
	}
	
	public function splithash($arg){
		$result = array();
		$arg = explode(" ", $arg);
		$needle = '#';
		foreach ($arg as $itm) {
			$len = strlen($itm);
			$pos = strpos($itm , $needle);
			if(($pos===0)and($len>=3)){
				$result[] = $itm;
			//$arg = preg_split("/[#@]/",$arg);
			}
		return $result;
		}
	}
	
	public function callstoaction($arg){
		$result = '';
		if($arg!=='z'){
			$cta = (1==1) ? 'Buy':'Book';
			$result = '<div><a href="" class="button">'.$cta.' '.self::endproduct($arg).'</a></div>';
		} else {
			$result = '<div><h2>Hi, Welcome to my Instagram eCommerce store, Wandaquila.com!</h2><p>All of the picture you see here will be cross referenced against Amazon, eBay, OpenTable, and a number of other online stores and booking sites, so you can buy these same items and book reservations at these same restaurants easily.  Just click the image you like, and details of the item, with links to the purchasing platform will slide out.  Click a picture to expand it and click it again to shrink it.  Happy shopping!</p></div>';
		}
		return $result;
	}

	public function listing(){
		$id = 0;
		$result = '';
		$allfilters = '';
		$filterarray = self::filterarray();
		$matches = array();
		foreach ($filterarray as $hashtag) {
			//$hashtag = self::cleaner($hashtag);
			$allfilters .= $hashtag.' ';
		}
		$result .= "\t".'<li class="'.$allfilters.' clearfilters firstlist"><div class="right cta">'.self::callstoaction('z').'</div><img src="/img/wanda.jpg"/></li>';
		foreach ($this->jsonData->data as $key=>$value) {
			if(isset($value->caption)){
				$description = $value->caption->text;
			}
			if(isset($value->tags)&&!empty($value->tags)){
				$matches = $value->tags;
				$tags = '';
				//$matches = self::splithash($hashtag);
				foreach ($matches as $match) {
					$match = self::cleaner($match);
					$tags .= $match.' ';
				};
				$timestamp = $value->caption->created_time;
			} else {
				$tags = '';
				$timestamp = time();
			}
			$result .= "\t".'<li id="list-'.$id.'" title="'.htmlentities($description).' '.htmlentities(date("F j, Y, g:i a", $timestamp)).'" class="clearfilters clearlarger '.$tags.'" href="'.$value->images->standard_resolution->url.'">
							<div class="right cta">'.self::callstoaction($id).'</div><img src="'.$value->images->low_resolution->url.'" alt="'.$description.'" /></li>';
			$id++;
		}
		echo $result;
	}
	
	public function sortArray(&$items, $key, $descending = false){
	  if (is_array($items)){
		return usort($items, function($a, $b) use ($key, $descending){
		  $cmp = $a['type']-$b['type'];
		  return $descending? -$cmp : $cmp;
		});
	  }
	  return false;
	}
	
	/*public function sortValue($a, $b) {
		//return strcasecmp(@$a->$itm->$metric, $b->$itm->$metric); 
		return $b['type'] - $a['type'];
	}
	
	public function filtermultiarray($type='date',$order=true){
			$desc = ($order=='descending')? true : false;
			if($type=='date'){
				$itm='caption';
				$metric='created_time';
			}else{
				$itm='likes';
				$metric='count';
			}
			$result = array();
			$data = $this->jsonData->data;
			foreach ($data as $key=>$value) {
				if(isset($value->$itm->$metric)){
					$result[] = array('tags'=>$value->tags,'type'=>$value->$itm->$metric);
				}
				else {
					$mt = array_fill(0,1,'... Whoops, sorry. Nothing available');
					$mt2 = array_fill(0,1,'0');
					$result[] = array('tags'=>$mt,'type'=>$mt2);
				}
			}
			//$results = usort($result, 'SortValue') use ($itm, $metric);
			usort($result,array($this, 'sortValue'));
			self::search_r($result, 'tags', $results);
			return $result;
	}*/

	public function filtermultiarray($type='date',$order=true){
			$desc = ($order=='descending')? true : false;
			if($type=='date'){
				$itm=false;
				$metric='created_time';
			}else{
				$itm='likes';
				$metric='count';
			}
			$result = array();
			$data = $this->jsonData->data;
			usort($data, function($a, $b) use ($itm, $metric, $desc) {
				$desc = ($desc) ? -1 : 1;
				if($itm!==false){
					return ($a->$itm->$metric > $b->$itm->$metric) ? $desc : ($desc*-1);
				} else {
					return ($a->$metric > $b->$metric) ? $desc : ($desc*-1);
				}
			});
			foreach ($data as $key=>$value) {
				if(isset($value->tags)){
					$result[] = $value->tags;
				} else {
					$result[] = '... Whoops, sorry. Nothing available';
				}
			}
			return $result;
	}
	
	public function filterarray($type='date',$number='100',$order='descending') {
		$result = array();
		$allfilters = self::filtermultiarray($type,$order);
		$i = 0;
		foreach ($allfilters as $key=>$value) {
			foreach ($value as $key=>$match) {
				//$matches = self::splithash($match);
				$i++;
				if($i>=intval($number)){
					break;
				} else {
					$result[] = $match;
				}
			}
		}
		return array_unique($result);
	}
	
	public function filt($arg){
		$result = self::filtermultiarray()[$arg];
		return $result;
	}
	
	public function filters($type='date',$number='100',$order='descending') {
		static $allfilters = '';
		$result = '';
		$result .= "\t".'<li title="clearfilters" class="clearfilters filter"><a id="clearfilters-'.$type.'" class="clearfilters-'.$type.'">Clear Filtered</a></li>';
		$result .= "\t".'<li title="clearlarger" class="clearlarger filter"><a id="clearlarger-'.$type.'" class="clearlarger-'.$type.'">Minimize Filtered</a></li>';
		$filterarray = self::filterarray($type,$number,$order);
		foreach ($filterarray as $hashtag) {
			$result .= "\t".'<li title="'.htmlentities($hashtag).'" class="filter"><a id="'.$hashtag.'" class="clearfilters-'.$type.' clearlarger-'.$type.'">'.$hashtag.'</a></li>';
		}
		echo $result;
	}
	
	public function endproduct($arg) {
		$result = '';
		$data = self::filt($arg);
		foreach ($data as $item) {
			$result .= $item.' ';
		}
		return $result;
	}
}

$instagram = new instagram('wanda2k4','86829003d4d8483e84fba6eaf612cee4'); //28601952

?>